// First part
let firstName = "Juan"
let lastName = "Dela Cruz"
let age = 21;
let currentAddress = {
    city: "Quezon City",
    province: "Metro Manila"
};

function bio(firstName, lastName, age, currentAddress) {
    return "Hello! I am " + firstName + " " + lastName + ", " + age + " years old, and currently living in " + currentAddress.city + ", " + currentAddress.province + ".";
 };
 console.log(bio(firstName, lastName, age, currentAddress));


//  Second part
let result;
function vote(age) {
    if (age >= 18) {
        result = "You can vote.";
    }
    else {
        result = "Sorry, You're too young to vote."
    }
    return result;
};
 console.log(vote(age));


//  Third part
let input = prompt("Enter month number: ");
input = parseInt(input);
switch (input) {
    // For 31 days
    case 1:
    console.log("Total Number of days for January: 31")
    break;

    case 3:
    console.log("Total Number of days for March: 31")
    break;

    case 5:
    console.log("Total Number of days for May: 31")
    break;
    
    case 7:
    console.log("Total Number of days for July: 31")
    break;

    case 8:
    console.log("Total Number of days for August: 31")
    break;

    case 10:
    console.log("Total Number of days for October: 31")
    break;

    case 12:
    console.log("Total Number of days for December: 31");
    break;

    // for 30 days
    case 4:
    console.log("Total Number of days for April: 30")
    break;

    case 6:
    console.log("Total Number of days for June: 30")
    break;

    case 9:
    console.log("Total Number of days for September: 30")
    break;

    case 11:
    console.log("Total Number of days for Novermber: 30");
    break;

    // For 28 days
    case 2:
    console.log("Total Number of days for February: 28");
    break

    default:
        alert("Invalid input! Please Enter the month number between 1-12");
    break;
};


// Forth Part
function leapYear() {
    let inputYear = prompt("Enter a year: ")
    if(inputYear % 400 || inputYear % 4 === 0) {
        return inputYear + " is a leap year";
    }
    else {
        return "Not a leap year"
    }
};
console.log(leapYear());


// Fifth part
let num = prompt("Enter a number: ");
num = parseInt(num);
function number(){
    for(let i = num; i >= 0; i--) {
        console.log(i);
    }
};
number();
